import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from "@/router/module/routes";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    // mode: 'hash',
    routes: [
        ...routes,
    ]
});
router.beforeEach((t, f, n) => {
    n();
});

export default router;
