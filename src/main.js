import Vue from 'vue'
import App from './App.vue'
import "@/plugins/index.plugin";
import store from "./store";
import router from "./router";
import {Instance} from "@/utils/project.util";
import {Navigate} from "@/utils/navigate.util";
import GlobalMixin from "@/mixin/GlobalMixin";

Vue.config.productionTip = false;
Vue.use(Navigate);
Vue.mixin(GlobalMixin);

new Vue({
    store,
    router,
    render: h => h(App),
    created() {
        Instance.VueApp = this;
    }
}).$mount('#app');
