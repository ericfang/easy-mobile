import {Events, Navigate} from "@/utils/navigate.util";

export default {
    data() {
        return {
            g_navigate: Navigate,
        }
    },
    destroy() {
        Events.invokeWaitTask();
    },
    deactivated() {
        Events.invokeWaitTask();
    },
    methods: {}
}