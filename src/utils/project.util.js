import {Dialog} from "vant";
import {Events} from "@/utils/navigate.util";

export const Instance = {
    VueApp: null,
};

/**
 * 提示信息
 */
export const DialogUtil = {
    toast(content, status = 'success') {
    },
    confirm(content, title = "确认信息", type = 'warning') {
        let dialog = Dialog.confirm({
            title: title,
            message: content,
        });
        let task = Events.addWaitTask(() => Dialog.close());
        return dialog.then(() => {
            console.log( 123 );
            Events.removeWaitTask(task);
            return Promise.resolve();
        }).catch(() => {
            Events.removeWaitTask(task);
            return Promise.reject();
        })
    },
    alert(content, title = "友情提示") {
        let dialog = Dialog.alert({
            title: title,
            message: content,
        });
        let task = Events.addWaitTask(() => Dialog.close());
        return dialog.then(() => {
            Events.removeWaitTask(task);
            return Promise.resolve();
        }).catch(() => {
            Events.removeWaitTask(task);
            return Promise.reject();
        })
    }
};
