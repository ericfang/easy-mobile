import numeral from 'numeral';
import Mock from 'mockjs';
import low from 'lowdb'
import LocalStorage from 'lowdb/adapters/LocalStorage'
import setting from "@/setting";
import lodash from "lodash";

export const numeralUtil = numeral;
export const _ = lodash;
export const mockUtil = Mock;

/**
 * router 载入
 * 在生产环境下使用懒加载
 * @param r
 * @returns {function(): *}
 */
export const _import = (r) => {
    if (process.env.NODE_ENV === 'production') {
        return () => import('@/views/' + r );
    } else {
        return require('@/views/' + r).default;
    }
};



/**
 * 返回创建好的 lowdb 实例
 * 使用浏览器本地存储
 */
export const lowDb = low(new LocalStorage('db'));


/**
 * 判断元素是否为空
 * @param v
 * @param zeroIsEmpty  0 是否为空
 * @returns {boolean}
 */
export const isEmpty = (v, zeroIsEmpty = false) => {
    if (typeof v === 'number') return v ? false : zeroIsEmpty;
    if (!v) return true;
    if (typeof v === 'object') return !Object.keys(v).length;
    return false;
};

/**
 * DOM 工具
 */
export const DomUtil = {
    // 样式参数转换为带单位的参数
    styleSize(v) {
        v += '';
        if (/^\d+$/.test(v)) return v + 'px';
        return v;
    }
};

export const FileUtil = {
    sizeString2Byte(str) {
        if (_.isNumber(str)) return str;
        str = str.toLowerCase();
        let [units, n1024] = [
            ['b', 'k', 'm', 'g', 't', 'p'],
            [0, 1, 2, 3, 4, 5],
        ];
        while (units.length) {
            let u = units.pop();
            let n = n1024.pop();
            if (str.indexOf(u) === -1) continue;
            str = str.replace(u, '');
            return (str * 1) * Math.pow(1024, n);
        }
        return str * 1;
    }
};

/**
 * mock 模拟数据
 * @param m
 */
export const mock = (m) => {
    return Mock.mock(m);
};

/**
 * mock 模拟数据
 * 直接传递对象以及数量即可
 * @param obj
 * @param num
 */
export const mockObj = (obj, num) => {
    if( _.isString(obj) ) return obj;
    let m = {};
    m[`list|${num}`] = [obj];
    return mock(m).list;
};

/**
 * 生成约定的 返回结果
 * @param obj
 * @param num
 * @param code
 * @returns {{code: number, data: *, message: string}}
 */
export const mockResponse = (obj, num, code = 200) => {
    return {
        code: code,
        message: 'success',
        data: mockObj(obj, num),
    }
};

/**
 * 配置URL拦截
 * @param url
 * @param obj
 * @param num
 * @param code
 * @param activated
 * @param method
 */
export const mockResponseIntercept = (url, obj, num, activated = true, code = 200, method = 'all') => {
    if (!activated) return;
    method = method === 'all' ? 'get|post' : method;
    let regex = new RegExp(method, 'i');
    Mock.mock(setting.API_BASE_URL + url, regex, mockResponse(obj, num, code));
};

/**
 * url编码
 * 将 JSON 格式转换成 key=value&key=value格式
 * 并将 value 做 url 编码
 * @param param
 * @param key
 * @returns {string}
 */
export const urlEncode = (param, key) => {
    let paramStr = "";
    if (typeof param === "string" || typeof param === 'number' || typeof param === 'boolean') {
        paramStr += "&" + key + "=" + encodeURIComponent(param);
    } else {
        for (let i in param) {
            if (!param.hasOwnProperty(i)) continue;
            let item = param[i];
            // let k = key == null ? i : key + (true || param instanceof Array ? "[" + i + "]" : "." + i);
            let k = key == null ? i : key + ("[" + i + "]");
            paramStr += '&' + urlEncode(item, k);
        }
    }
    return paramStr.substr(1);
};



/**
 * 列表 转换为 树状结构
 * @param list
 * @param pid
 * @param childKey
 * @param pidKey
 * @param idKey
 * @returns {[]}
 */
export const list2Tree = (list, pid = '0', {childKey = 'children', pidKey = 'pid', idKey = 'id'} = {}) => {
    let res = [];
    list.forEach(l => {
        if (l[pidKey] === pid) {
            l[childKey] = list2Tree(list, l[idKey], {
                childKey: childKey,
                pidKey: pidKey,
                idKey: idKey,
            });
            if (!l[childKey].length) {
                delete l[childKey];
            }
            res.push(l);
        }
    });
    return res;
};

/**
 * 从树状结构中查找内容
 * @param tree
 * @param cb
 * @param onlyLeaf
 * @param children
 * @returns {[]}
 */
export const treeFilter = (tree, cb, onlyLeaf = false, children = 'children') => {
    let res = [];
    tree.forEach(x => {
        if (cb(x)) {
            if (!onlyLeaf) {
                res.push(x);
            } else if (!x.children || !x.children.length) {
                res.push(x);
            }
        }
        if (x.children) {
            res = res.concat(treeFilter(x.children, cb, onlyLeaf, children));
        }
    });
    return res;
};
/**
 * 将两个list 合并为一个树状结构
 * @param list1
 * @param list2
 * @param idKey
 * @param pidKey
 * @param childKey
 * @returns {*}
 */
export const listMerge2Tree = (list1, list2, idKey = 'id', pidKey = 'pid', childKey = 'children') => {
    list1.forEach(row => {
        row[childKey] = list2.filter(r2 => r2[pidKey] === row[idKey]) || [];
    });
    return list1;
};


/**
 * 查找树状结构轨迹
 * @param tree
 * @param cb
 * @param keyName
 * @param children
 * @returns {[]}
 */
export const treeTrace = (tree = [], cb, keyName = 'id', children = 'children') => {
    let res = [];
    for (let i in tree) {
        let D = tree[i];
        if (cb(D)) {
            res.push(D[keyName]);
            break;
        }
        if (D[children] && D[children].length) {
            let childRes = treeTrace(D[children], cb, keyName, children);
            if (childRes.length) {
                res.push(D[keyName]);
                res = res.concat(childRes);
                break;
            }
        }
    }
    return res;
};

/**
 * 根据集合key进行去重
 * @param collection
 * @param fieldMethod
 * @returns {[]}
 */
export const collectionRemoveDuplication = (collection, fieldMethod) => {
    let fieldSet = new Set();
    let res = [];
    collection.forEach(x => {
        if (fieldSet.has(fieldMethod(x))) return;
        res.push(x);
        fieldSet.add(fieldMethod(x));
    });
    return res;
};

/**
 * 从 label - value 中获取 label
 * @param list
 * @param value
 * @returns {*}
 */
export const getLabelFromSelector = (list, value) => {
    let item = list.find(x => x.value === value);
    return item ? item.label : "";
};

/**
 * 将过滤条件加上表格前缀
 * 后端默认以 t 作为主表名称
 * @param filterForm
 * @param prefix
 * @returns {{}}
 */
export const convertFilterForm = (filterForm, prefix = 't.') => {
    let res = {};
    for (let i in filterForm) {
        if (!filterForm.hasOwnProperty(i)) continue;
        let D = filterForm[i];
        if (!i.includes('.')) {
            res[prefix + i] = D;
        } else {
            res[i] = D;
        }
    }
    return res;
};

/**
 * 从树状结构中找到元素
 * @param tree
 * @param cb
 * @param childKey
 * @returns {null|*}
 */
export const findInTree = (tree, cb, {childKey = "children"} = {}) => {
    for (let i = 0; i < tree.length; i++) {
        let D = tree[i];
        if (cb(D)) return D;
        if (D.children && D.children.length) {
            return findInTree(tree, cb, {childKey: childKey});
        }
    }
    return null;
};

// 下划线转换驼峰
export const toHump = (name) => {
    return name.replace(/\_(\w)/g, function (all, letter) {
        return letter.toUpperCase();
    });
};

// 驼峰转换下划线
export const toLine = (name) => {
    return name.replace(/([A-Z])/g, "_$1").toLowerCase();
};

/**
 * 数组是否一样
 * @param arr1
 * @param arr2
 * @returns {boolean}
 */
export const arraySame = (arr1, arr2) => {
    if (arr1.length !== arr2.length) return false;
    for (let i = 0; i < arr1.length; i++) {
        if (!arr2.find(x => x === arr1[i])) {
            return false;
        }
    }
    return true;
};

/**
 * 修改 web 标题
 * @param title
 */
export const changeWebTitle = (title) => {
    document.querySelector("title").innerHTML = title;
};
import {v4 as uuidv4} from 'uuid';

/**
 * 创建随机 uuid
 * v4: 随机
 * v1: 时间戳
 * @returns {*}
 */
export const createUUID = () => {
    return uuidv4();
};


/**
 * 限流和节流
 */
export const debThr = {
    /**
     * 默认节流和防抖动延迟时间
     */
    _defDelay: 100,
    /**
     * 节流阀缓存
     */
    _thrKey: {},
    /**
     * 防抖动缓存
     */
    _debKey: {},
    /**
     * 仅执行一次
     */
    _onceKey: {},

    /**
     * 一次性执行完成不再执行
     * @param key
     * @param callback
     */
    once(key, callback) {
        if (this._onceKey[key]) return;
        this._onceKey[key] = true;
        callback && callback();
    },
    /**
     * 节流阀
     * @param key
     * @param callback
     * @param delay
     */
    throttle(key, callback, delay = this._defDelay) {
        if (this._thrKey[key]) return;
        this._thrKey[key] = true;
        callback && callback();
        setTimeout(() => {
            this._thrKey[key] = false;
        }, delay);
    },

    /**
     * 防抖动
     * @param key
     * @param callback
     * @param delay
     */
    debounce(key, callback, delay = this._defDelay + 1) {
        if (this._debKey[key]) clearTimeout(this._debKey[key]);
        this._debKey[key] = setTimeout(() => {
            callback && callback();
        }, delay);
    },

    /**
     * 节流阀与防抖动一起使用
     * @param key
     * @param func
     * @param delay
     */
    thrAndDeb(key, func, delay) {
        delay = delay === null ? 100 : delay;
        this.throttle(key, func, delay);
        this.debounce(key, func, delay);
    },

    /**
     * 节流阀 函数表达式返回
     * @param func  函数
     * @param delay 节流延迟
     * @returns {Function}
     */
    throttleFunction(func, delay = this._defDelay) {
        let isValid = true;
        return (...params) => {
            if (!isValid) return;
            isValid = false;
            func && func(...params);
            setTimeout(() => {
                isValid = true;
            }, delay);
        };
    },
    /**
     * 防抖动 函数表达式返回
     * @param func  函数
     * @param delay 防抖延迟
     * @returns {Function}
     */
    debounceFunction(func, delay = this._defDelay + 1) {
        let timer;
        return (...params) => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                func && func(...params);
            }, delay);
        }
    },
    /**
     * 节流阀和防抖动函数表达式返回
     * @param func      函数
     * @param delayThrottle 节流延迟
     * @param delayDebounce 防抖延迟
     * @returns {Function}
     */
    thrAndDebFunction(func, delayThrottle = null, delayDebounce = null) {
        let throttleFunc = this.throttle(func, delayThrottle);
        let debounceFunc = this.debounce(func, delayDebounce);
        return (...params) => {
            throttleFunc(...params);
            debounceFunc(...params);
        }
    },
};